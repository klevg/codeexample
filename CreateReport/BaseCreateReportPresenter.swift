//
//  BaseCreateReportPresenter.swift
//  RadiusReport
//
//  Created by Eugene on 2/19/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

protocol IBaseCreateReportPresenter: ViewLifecycleSupportable {
    func setHeadlineViewConfig(view: ICreateReportHeadlineView)
    func setLocationViewConfig(view: ICreateReportAddressView)

    func sendReport()
    var onCancel: (() -> Void)? { get set }
}

class BaseCreateReportPresenter: IBaseCreateReportPresenter {
    struct LocationModel {
        let location: String
        let latitude: String
        let longitude: String
        let isCurrentLocation: Bool
    }

    var interactor: CreateReportInteractorInput!
    let locationService = GeolocationUseCase()

    var onCancel: (() -> Void)?
    var onSelect: (() -> Void)?
    var onLocation: (((@escaping (LocationModel) -> Void)) -> Void)?

    var headlineString = ""
    var location: String?
    var latitude = ""
    var longitude = ""

    func setHeadlineViewConfig(view: ICreateReportHeadlineView) {
        let config = CreateReportHeadlineView.Config(placeholderText: "Short Headline",
                                                     textDidChangedInRange: { [weak self] string in
            self?.headlineString = string
        })
        view.setup(with: config)
    }

    func setLocationViewConfig(view: ICreateReportAddressView) {
        let config = CreateReportAddressView.Config(leftTitle: "Where is this?",
                                                    locationTitle: location ?? "Current Location",
                                                    didTapped: { [weak self] in
                guard let self = self else { return }
                self.onLocation?(self.updateModel)
        })
        view.setup(with: config)
    }

    func updateModel(model: LocationModel) {
        location = model.location
        self.latitude = model.latitude
        self.longitude = model.longitude
    }

    func sendReport() {
        fatalError("must be overriden")
    }
}
