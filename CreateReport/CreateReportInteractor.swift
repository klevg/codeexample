//
//  CreateReportInteractor.swift
//  RadiusReport
//
//  Created by Eugene on 2/17/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

protocol CreateReportInteractorInput {
    func send(model: ReportRequest)
}

protocol CreateReportInteractorOutput: class {
    func success()
    func failure()
    func updateProgress(with: Double)
}

final class CreateReportInteractor {
    weak var output: CreateReportInteractorOutput?
    private let reportRepository: ReportRepository

    required init(reportRepository: ReportRepository) {
        self.reportRepository = reportRepository
    }
}

// MARK: - CreateReportInteractorInput
extension CreateReportInteractor: CreateReportInteractorInput {
    func send(model: ReportRequest) {
        reportRepository.createReport(model,
                                      progressDelegate: self,
                                      completion: { [weak self] result in
            switch result {
            case .success(let response):
                self?.output?.success()
                print("response = \(response)")
            case .failure(let error):
                self?.output?.failure()
                print("error = \(error)")
            }
        })
    }
}

// MARK: - APIProviderDelegate
extension CreateReportInteractor: APIProviderDelegate {
    func didUpdateProgress(with: Double) {
        output?.updateProgress(with: with)
    }
}
