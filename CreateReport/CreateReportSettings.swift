//
//  CameraSettings.swift
//  RadiusReport
//
//  Created by Eugene on 2/27/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import CoreLocation

final class CreateReportSettings {
    private init() {}

    static let videoDuration: TimeInterval = 30
    static let regionRadius: CLLocationDegrees = 0.01
}
