//
//  CreateReportWithLinkAssembly.swift
//  Development
//
//  Created by Eugene on 2/10/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

final class CreateReportWithLinkAssembly {
    struct Module {
        let view: CreateReportWithLinkController
        let presenter: CreateReportWithLinkPresenter
    }

    private init() {}

    static func build(reportRepository: ReportRepository) -> Module {
        let view = CreateReportWithLinkController()
        let presenter = CreateReportWithLinkPresenter(view: view)
        view.presenter = presenter
        let interactor = CreateReportInteractor(reportRepository: reportRepository)
        interactor.output = presenter
        presenter.interactor = interactor
        return .init(view: view, presenter: presenter)
    }
}
