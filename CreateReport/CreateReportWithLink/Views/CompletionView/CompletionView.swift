//
//  ComplitionView.swift
//  RadiusReport
//
//  Created by Eugene on 2/17/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit

final class CompletionView: XibView {
    enum State {
        case success
        case failure

        var text: String {
            switch self {
            case .success:
                return "Your report was posted!"
            case .failure:
                return "Your report posting failed!"
            }
        }
    }

    static let size = CGSize(width: 246,
                             height: 46)
    static let animationDelay = 3
    static let animationDuration = 1.0

    private let cornerRadius: CGFloat = 15
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        containerView.layer.cornerRadius = cornerRadius
        containerView.backgroundColor = .radiusRedOrange
    }

    // MARK: - Setup view
    func setupWith(state: State) {
        textLabel.text = state.text
    }

    static func show(on tabbarVC: UITabBarController, completion: @escaping (() -> Void)) {
        let size = CompletionView.size
        let yPosition = tabbarVC.view.frame.height - size.height  - tabbarVC.tabBar.frame.height - 20
        let xPosition = tabbarVC.view.center.x - size.width / 2

        let completionView = CompletionView(frame: CGRect(x: xPosition,
                                                          y: yPosition,
                                                          width: size.width,
                                                          height: size.height))
        completionView.setupWith(state: .success)
        tabbarVC.view.addSubview(completionView)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(animationDelay),
                                      execute: {
            UIView.animate(withDuration: animationDuration,
                           animations: {
                completionView.frame = CGRect(x: -size.width,
                                              y: yPosition,
                                              width: size.width,
                                              height: size.height)
            },
                           completion: { _ in
                completionView.removeFromSuperview()
                completion()
            })
        })
    }
}
