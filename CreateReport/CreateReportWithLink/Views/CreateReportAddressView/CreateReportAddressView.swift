//
//  CreateReportAddressCell.swift
//  RadiusReport
//
//  Created by Eugene on 2/7/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit

protocol ICreateReportAddressView: class {
    func setup(with config: CreateReportAddressView.Config)
    func update(with title: String?)
}

final class CreateReportAddressView: XibView, ICreateReportAddressView {
    final class Config {
        let leftTitle: String
        let locationTitle: String
        var didTapped: (() -> Void)?

        init(leftTitle: String,
             locationTitle: String,
             didTapped: (() -> Void)?) {
            self.leftTitle = leftTitle
            self.locationTitle = locationTitle
            self.didTapped = didTapped
        }
    }

    static let height: CGFloat = 57

    @IBOutlet private weak var leftTitleLabel: UILabel!
    @IBOutlet private weak var locationTitleLabel: UILabel!

    private var didTapped: (() -> Void)?

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        contentView?.addGestureRecognizer(tap)
    }

    // MARK: - Action
    @objc private func handleTap() {
        didTapped?()
    }

    // MARK: - SetupView
    func setup(with config: Config) {
        leftTitleLabel.text = config.leftTitle
        locationTitleLabel.text = config.locationTitle
        didTapped = config.didTapped
    }

    func update(with title: String?) {
        locationTitleLabel.text = title
    }
}
