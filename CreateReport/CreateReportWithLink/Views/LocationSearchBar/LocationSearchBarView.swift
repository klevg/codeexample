//
//  LocationSearchBar.swift
//  RadiusReport
//
//  Created by Eugene on 2/12/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit

protocol LocationSearchBarViewDelegate: class {
    func textDidChanged(value: String)
}

final class LocationSearchBarView: XibView {
    final class Config {
        var didChanged: ((String) -> Void)?

        init(didChanged: ((String) -> Void)?) {
            self.didChanged = didChanged
        }
    }

    static let height: CGFloat = 54
    private let containerCornerRadius: CGFloat = 12

    @IBOutlet private var textField: UITextField!
    @IBOutlet private var containerView: UIView!

    weak var delegate: LocationSearchBarViewDelegate?

    @discardableResult
    override func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }

    @discardableResult
    override func resignFirstResponder() -> Bool {
        return textField.resignFirstResponder()
    }

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        containerView.layer.cornerRadius = containerCornerRadius
        textField.delegate = self
        textField.addDoneAccessoryView()
        textField.placeholder = "Enter text"
    }

    // MARK: - SetupView
    func setupWith(text: String?) {
        textField.text = text
    }
}

// MARK: - UITextFieldDelegate
extension LocationSearchBarView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        delegate?.textDidChanged(value: updatedString)
        return true
    }
}
