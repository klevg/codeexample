//
//  AddLinkView.swift
//  Development
//
//  Created by Eugene on 2/10/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit

protocol IAddTextOrLinkView: class {
    func setup(with config: AddTextOrLinkView.Config)
}

final class AddTextOrLinkView: XibView, IAddTextOrLinkView {
    final class Config {
        let placeholderText: String
        let image: UIImage?
        var textDidChanged: ((String) -> Void)?

        init(placeholderText: String,
             image: UIImage? = nil,
             textDidChanged: ((String) -> Void)?) {
            self.placeholderText = placeholderText
            self.image = image
            self.textDidChanged = textDidChanged
        }
    }

    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var addLinkLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var stackView: UIStackView!

    private var textDidChanged: ((String) -> Void)?

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        textView.addDoneAccessoryView()
        textView.delegate = self
    }

    // MARK: - SetupView
    func setup(with config: Config) {
        addLinkLabel.text = config.placeholderText
        setupImageView(with: config.image)
        textDidChanged = config.textDidChanged
    }

    private func setupImageView(with image: UIImage?) {
        if let image = image {
            stackView.spacing = 6
            iconImageView.isHidden = false
            iconImageView.image = image
        } else {
            stackView.spacing = 0
            iconImageView.isHidden = true
        }
    }
}

// MARK: - UITextViewDelegate
extension AddTextOrLinkView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let updatedString = (textView.text as NSString?)?.replacingCharacters(in: range, with: text) ?? ""
        addLinkLabel.isHidden = !updatedString.isEmpty
        textDidChanged?(updatedString)
        return true
    }
}
