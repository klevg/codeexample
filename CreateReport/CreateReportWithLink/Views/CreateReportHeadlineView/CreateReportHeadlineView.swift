//
//  CreateReportHeadlineCell.swift
//  RadiusReport
//
//  Created by Eugene on 2/7/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit

protocol ICreateReportHeadlineView {
    func setup(with config: CreateReportHeadlineView.Config)
}

final class CreateReportHeadlineView: XibView, ICreateReportHeadlineView {
    final class Config {
        let placeholderText: String
        var textDidChangedInRange: ((String) -> Void)?

        init(placeholderText: String,
             textDidChangedInRange: ((String) -> Void)?) {
            self.placeholderText = placeholderText
            self.textDidChangedInRange = textDidChangedInRange
        }
    }

    static let height: CGFloat = 52

    @IBOutlet private weak var textField: UITextField!

    private var textDidChangedInRange: ((String) -> Void)?

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        textField.addDoneAccessoryView()
        textField.delegate = self
    }

    // MARK: - SetupView
    func setup(with config: Config) {
        textDidChangedInRange = config.textDidChangedInRange
        textField.placeholder = config.placeholderText
    }
}

// MARK: - UITextViewDelegate
extension CreateReportHeadlineView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        textDidChangedInRange?(updatedString)
        return true
    }
}
