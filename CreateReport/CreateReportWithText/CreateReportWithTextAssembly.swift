//
//  CreateReportWithTextAssembly.swift
//  RadiusReport
//
//  Created by Eugene on 2/19/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

final class CreateReportWithTextAssembly {
    struct Module {
        let view: CreateReportWithTextController
        let presenter: CreateReportWithTextPresenter
    }

    private init() {}

    static func build(reportRepository: ReportRepository) -> Module {
        let view = CreateReportWithTextController()
        let presenter = CreateReportWithTextPresenter(view: view)
        view.presenter = presenter
        let interactor = CreateReportInteractor(reportRepository: reportRepository)
        interactor.output = presenter
        presenter.interactor = interactor
        return .init(view: view, presenter: presenter)
    }
}
