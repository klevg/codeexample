//
//  CreateReportWithTextController.swift
//  RadiusReport
//
//  Created by Eugene on 2/19/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

final class CreateReportWithTextController: BaseCreateReportController,
                                            IBaseCreateReportController {
    var presenter: ICreateReportWithTextPresenter!

    private lazy var addTextOrLinkView: AddTextOrLinkView = {
        let view = AddTextOrLinkView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setHeadlineViewConfig(view: headlineView)
        presenter.setLocationViewConfig(view: addressView)
        presenter.setAddLinkViewConfig(view: addTextOrLinkView)
        presenter.viewDidLoad?()
    }

    private func setupLayout() {
        contentView.addSubview(addTextOrLinkView)
        NSLayoutConstraint.activate([
            addTextOrLinkView.topAnchor.constraint(equalTo: headlineView.bottomAnchor),
            addTextOrLinkView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            addTextOrLinkView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            addTextOrLinkView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }

    @objc override func leftBarButtonTapped() {
        presenter.onCancel?()
    }

    @objc override func rightBarButtonTapped() {
        presenter.sendReport()
    }
}
