//
//  Presenter.swift
//  RadiusReport
//
//  Created by Eugene on 2/19/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

protocol ICreateReportWithTextPresenter: IBaseCreateReportPresenter {
    func setAddLinkViewConfig(view: IAddTextOrLinkView)
}

final class CreateReportWithTextPresenter: BaseCreateReportPresenter, ICreateReportWithTextPresenter {
    private weak var view: IBaseCreateReportController?

    private var textString = ""

    init(view: IBaseCreateReportController) {
        self.view = view
    }

    func viewDidLoad() {
        view?.setTitle(with: "Post Report")
        if locationService.isGranted {
            locationService.onUpdateLocation = { [weak self] location in
                self?.locationService.stopUpdatingLocation()
                self?.locationService.geocode(coordinate: location.coordinate) { (locationString) in
                    let model = LocationModel(location: locationString ?? "",
                                              latitude: String(location.coordinate.latitude),
                                              longitude: String(location.coordinate.longitude),
                                              isCurrentLocation: true)
                    self?.updateModel(model: model)
                }
            }
            locationService.startUpdatingLocation()
        } else {
            view?.updateLocationTitle(with: "no location")
        }
    }

    override func sendReport() {
        view?.showHUD()
        let model = ReportRequest(title: headlineString,
                                  fullText: textString,
                                  link: nil,
                                  location: location ?? "",
                                  latitude: latitude,
                                  longitude: longitude,
                                  type: ReportType.text.index,
                                  deletedMedia: nil,
                                  media: nil)
        interactor.send(model: model)
    }

    override func updateModel(model: LocationModel) {
        super.updateModel(model: model)
        let locationTitle = model.isCurrentLocation ? "Current Location" : model.location
        view?.updateLocationTitle(with: locationTitle)
    }

    func setAddLinkViewConfig(view: IAddTextOrLinkView) {
        let config = AddTextOrLinkView.Config(placeholderText: "Full Text",
                                              textDidChanged: { [weak self] string in
            self?.textString = string
        })
        view.setup(with: config)
    }
}

// MARK: - CreateReportInteractorOutput
extension CreateReportWithTextPresenter: CreateReportInteractorOutput {
    func updateProgress(with: Double) {
        view?.showHUDWithProgress(with, title: "Uploading")
    }

    func success() {
        view?.hideHUD()
        onSelect?()
    }

    func failure() {
        view?.hideHUD()
        view?.showErrorAlert()
    }
}
