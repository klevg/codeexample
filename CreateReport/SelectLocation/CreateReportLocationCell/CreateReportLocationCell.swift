//
//  CreateReportLocationCell.swift
//  RadiusReport
//
//  Created by Eugene on 2/13/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit

final class CreateReportLocationCell: UITableViewCell, TableViewNibCell {
    static let height: CGFloat = 50

    @IBOutlet private weak var locationLabel: UILabel!

    func setupWith(text: String) {
        locationLabel.text = text
    }
}
