//
//  SelectLocationTableDirector.swift
//  RadiusReport
//
//  Created by Eugene on 2/13/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import MapKit

protocol CreateReportSelectLocationTableDirectorDelegate: class {
    func didSelect(at index: Int)
}

final class CreateReportSelectLocationTableDirector: NSObject, UITableViewDelegate, UITableViewDataSource {
    weak var delegate: CreateReportSelectLocationTableDirectorDelegate?
    var searchResults = [String]()

// MARK: - UITableViewDelegate, UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithRegistration(type: CreateReportLocationCell.self, indexPath: indexPath)
        let searchResult = searchResults[indexPath.row]
        cell.setupWith(text: searchResult)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.didSelect(at: indexPath.row)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CreateReportLocationCell.height
    }
}
