//
//  CreateReportSelectLocationBuilder.swift
//  RadiusReport
//
//  Created by Eugene on 2/12/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

struct CreateReportSelectLocationAssembly {
    struct Module {
        let view: CreateReportSelectLocationController
        let presenter: CreateReportSelectLocationPresenter
    }

    private init() {}

    static func build(updateModelFunction: @escaping (BaseCreateReportPresenter.LocationModel) -> Void) -> Module {
        let view = CreateReportSelectLocationController()
        let tableDirector = CreateReportSelectLocationTableDirector()
        let presenter = CreateReportSelectLocationPresenter(view: view,
                                                            tableDirector: tableDirector,
                                                            updateModelFunction: updateModelFunction)
        view.presenter = presenter
        let interactor = CreateReportSelectLocationInteractor()
        interactor.output = presenter
        presenter.interactor = interactor

        return .init(view: view, presenter: presenter)
    }
}
