//
//  CreateReportSelectLocation.swift
//  RadiusReport
//
//  Created by Eugene on 2/12/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit
import MapKit

protocol ICreateReportSelectLocationController: class {
    func setTableViewHeightWithCount(of items: Int)
    func setupDelegate(tableDirector: CreateReportSelectLocationTableDirector)
    func setMapIn(point: CLLocationCoordinate2D)
    func setSearchTitleWith(text: String?)
    func setupMyLocationButton()
}

final class CreateReportSelectLocationController: UIViewController {

    var presenter: ICreateReportSelectLocationPresenter!
    private let dragToAdjustAnimationDuration = 0.6
    private let dragToAdjustCornerRadius: CGFloat = 4

    private var tableViewHeightConstraint: NSLayoutConstraint!

    private lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.delegate = self
        return mapView
    }()

    private lazy var searchBar: LocationSearchBarView = {
        let view = LocationSearchBarView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.allowsSelection = true
        tableView.isUserInteractionEnabled = true
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(.imageFrom(key: .locationDone), for: .normal)
        button.addTarget(self, action: #selector(rightBarButtonTapped), for: .touchUpInside)
        return button
    }()

    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(.imageFrom(key: .locationClose), for: .normal)
        button.addTarget(self, action: #selector(leftBarButtonTapped), for: .touchUpInside)
        return button
    }()

    private lazy var myLocationButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(.imageFrom(key: .currentLocation), for: .normal)
        button.addTarget(self, action: #selector(myLocationButtonTapped), for: .touchUpInside)
        return button
    }()

    private var dragToAdjustView: UIView?
    private var mapIsLoad = false
    private var isDragToAdjustViewHidden = false

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        view.addSubview(mapView)
        view.addSubview(searchBar)
        view.addSubview(confirmButton)
        view.addSubview(cancelButton)

        let imageView = UIImageView(image: .imageFrom(key: .target))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        mapView.addSubview(imageView)

        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            searchBar.heightAnchor.constraint(equalToConstant: LocationSearchBarView.height),
            searchBar.topAnchor.constraint(equalTo: guide.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            searchBar.bottomAnchor.constraint(equalTo: mapView.topAnchor),
            mapView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            imageView.centerYAnchor.constraint(equalTo: mapView.centerYAnchor),
            imageView.centerXAnchor.constraint(equalTo: mapView.centerXAnchor),

            cancelButton.heightAnchor.constraint(equalToConstant: 54),
            cancelButton.widthAnchor.constraint(equalToConstant: 54),
            cancelButton.leadingAnchor.constraint(equalTo: mapView.leadingAnchor, constant: 11),
            cancelButton.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: -22),

            confirmButton.heightAnchor.constraint(equalToConstant: 54),
            confirmButton.widthAnchor.constraint(equalToConstant: 54),
            confirmButton.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -11),
            confirmButton.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: -22)
        ])

        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        tableViewHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0)
        tableViewHeightConstraint.isActive = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad?()
        title = "Mark Location"

        searchBar.delegate = presenter
        setupBarButtonItems()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.shouldRemoveShadow(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.shouldRemoveShadow(false)
    }

    private func setupBarButtonItems() {
        let leftButton = UIBarButtonItem(title: "Cancel",
                                         style: .plain,
                                         target: self,
                                         action: #selector(leftBarButtonTapped))
        let leftButtonColor = UIColor.radiusInactiveGrey
        leftButton.setTitleTextAttributes([.font: UIFont.mediumFont(ofSize: 17),
                                           .foregroundColor: leftButtonColor], for: .normal)
        navigationItem.leftBarButtonItem = leftButton

        let rightButton = UIBarButtonItem(title: "Select",
                                          style: .plain,
                                          target: self,
                                          action: #selector(rightBarButtonTapped))
        let rightButtonColor = UIColor.radiusVibrantGreen
        rightButton.setTitleTextAttributes([.font: UIFont.mediumFont(ofSize: 17),
                                            .foregroundColor: rightButtonColor], for: .normal)
        navigationItem.rightBarButtonItem = rightButton
    }

    // MARK: - Actions
    @objc private func leftBarButtonTapped() {
        presenter.onBack?()
    }

    @objc private func rightBarButtonTapped() {
        presenter.selectButtonTapped()
    }

    @objc private func myLocationButtonTapped() {
        presenter.showMyLocation()
    }

    private func setupDragToAdjustView() {
        dragToAdjustView = UIView()
        dragToAdjustView?.translatesAutoresizingMaskIntoConstraints = false
        dragToAdjustView?.layer.cornerRadius = dragToAdjustCornerRadius
        dragToAdjustView?.backgroundColor = .radiusBrownGrey
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.center.x = view.center.x
        label.center.y = view.center.y
        label.text = "Drag Map To Adjust"
        label.font = UIFont.mediumFont(ofSize: 14)
        label.textColor = .white
        guard let dragToAdjustView = dragToAdjustView else { return }
        mapView.addSubview(dragToAdjustView)
        dragToAdjustView.addSubview(label)

        NSLayoutConstraint.activate([
            dragToAdjustView.centerXAnchor.constraint(equalTo: mapView.centerXAnchor),
            dragToAdjustView.centerYAnchor.constraint(equalTo: cancelButton.centerYAnchor),
            dragToAdjustView.heightAnchor.constraint(equalToConstant: 36),
            dragToAdjustView.widthAnchor.constraint(equalToConstant: 140),

            label.centerYAnchor.constraint(equalTo: dragToAdjustView.centerYAnchor),
            label.centerXAnchor.constraint(equalTo: dragToAdjustView.centerXAnchor)
        ])
    }
}

// MARK: - ICreateReportSelectLocationController
extension CreateReportSelectLocationController: ICreateReportSelectLocationController {
    func setupDelegate(tableDirector: CreateReportSelectLocationTableDirector) {
        tableView.delegate = tableDirector
        tableView.dataSource = tableDirector
    }

    func setTableViewHeightWithCount(of items: Int) {
        tableViewHeightConstraint.constant = CreateReportLocationCell.height * CGFloat(items)
        if items > 0 {
            tableView.reloadData()
        }
    }

    func setMapIn(point: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion(center: point,
                                        span: MKCoordinateSpan(latitudeDelta: CreateReportSettings.regionRadius,
                                                               longitudeDelta: CreateReportSettings.regionRadius))
        mapView.setRegion(region, animated: true)
    }

    func setSearchTitleWith(text: String?) {
        searchBar.setupWith(text: text)
    }

    func setupMyLocationButton() {
        view.addSubview(myLocationButton)

        NSLayoutConstraint.activate([
            myLocationButton.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 23),
            mapView.trailingAnchor.constraint(equalTo: myLocationButton.trailingAnchor, constant: 13),
            myLocationButton.heightAnchor.constraint(equalToConstant: 28),
            myLocationButton.widthAnchor.constraint(equalToConstant: 28)
        ])
    }
}

// MARK: - MKMapViewDelegate
extension CreateReportSelectLocationController: MKMapViewDelegate {
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        if !mapIsLoad {
            setupDragToAdjustView()
            mapIsLoad.toggle()
        }
    }

    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        if dragToAdjustView != nil, !isDragToAdjustViewHidden {
            isDragToAdjustViewHidden = true
            UIView.animate(withDuration: dragToAdjustAnimationDuration,
                           delay: 0,
                           options: [.curveEaseIn],
                           animations: {
                self.dragToAdjustView!.alpha = 0
            },
                           completion: { _ in
                self.dragToAdjustView!.removeFromSuperview()
                self.dragToAdjustView = nil
            })
        }
        setTableViewHeightWithCount(of: 0)
        searchBar.resignFirstResponder()
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        presenter.geocode(coordinate: mapView.centerCoordinate)
        presenter.currentMapCoordinate = mapView.centerCoordinate
    }
}
