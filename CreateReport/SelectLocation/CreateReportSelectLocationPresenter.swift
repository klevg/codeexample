//
//  CreateReportSelectLocationPresenter.swift
//  Development
//
//  Created by Eugene on 2/12/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import MapKit

protocol ICreateReportSelectLocationPresenter: ViewLifecycleSupportable,
                                               LocationSearchBarViewDelegate {
    var onBack: (() -> Void)? { get set }
    var currentMapCoordinate: CLLocationCoordinate2D { get set }

    func selectButtonTapped()
    func showMyLocation()
    func geocode(coordinate: CLLocationCoordinate2D)
}

final class CreateReportSelectLocationPresenter {
    private weak var view: ICreateReportSelectLocationController?

    var onBack: (() -> Void)?

    private let tableDirector: CreateReportSelectLocationTableDirector
    var interactor: CreateReportSelectLocationInteractorInput?
    private let locationService: GeolocationUseCase

    private var location: CLLocationCoordinate2D!
    private var address = ""
    private let updateModelFunction: (BaseCreateReportPresenter.LocationModel) -> Void
    private var isShowingCurrentLocation = false
    private var isLocationButtonSet = false
    private var userLocationCoordinate: CLLocationCoordinate2D?
    var currentMapCoordinate = CLLocationCoordinate2D() {
        didSet {
            if let userLocationCoordinate = userLocationCoordinate {
                let numberOfPlaces = 4.0 // to reduce coordinate accuracy
                let multiplier = pow(10.0, numberOfPlaces)

                let userLocationCoordinateLatitude = round(userLocationCoordinate.latitude * multiplier) / multiplier
                let userLocationCoordinateLongitude = round(userLocationCoordinate.longitude * multiplier) / multiplier
                let currentMapCoordinateLatitude = round(currentMapCoordinate.latitude * multiplier) / multiplier
                let currentMapCoordinateLongitude = round(currentMapCoordinate.longitude * multiplier) / multiplier
                if userLocationCoordinateLatitude == currentMapCoordinateLatitude &&
                    userLocationCoordinateLongitude == currentMapCoordinateLongitude {
                    view?.setSearchTitleWith(text: "Current location")
                    isShowingCurrentLocation = true
                    return
                }
            }
            isShowingCurrentLocation = false
        }
    }

    init(view: ICreateReportSelectLocationController,
         tableDirector: CreateReportSelectLocationTableDirector,
         updateModelFunction: @escaping (BaseCreateReportPresenter.LocationModel) -> Void) {
        self.view = view
        self.tableDirector = tableDirector
        self.updateModelFunction = updateModelFunction
        self.locationService = GeolocationUseCase()
    }

    func viewDidLoad() {
        tableDirector.delegate = self
        view?.setupDelegate(tableDirector: tableDirector)
        locationService.startUpdatingLocation()
        locationService.onUpdateLocation = { [weak self] location in
            guard let self = self else { return }
            self.locationService.stopUpdatingLocation()
            self.userLocationCoordinate = location.coordinate
            if !self.isLocationButtonSet {
                self.isLocationButtonSet = true
                self.view?.setupMyLocationButton()
            }
            self.view?.setMapIn(point: location.coordinate)
        }
    }

    func geocode(coordinate: CLLocationCoordinate2D) {
        location = coordinate
        interactor?.geocode(coordinate: coordinate)
    }
}

// MARK: - ICreateReportSelectLocationPresenter
extension CreateReportSelectLocationPresenter: ICreateReportSelectLocationPresenter {
    func selectButtonTapped() {
        let model = BaseCreateReportPresenter.LocationModel(location: address,
                                                            latitude: String(location.latitude),
                                                            longitude: String(location.longitude),
                                                            isCurrentLocation: isShowingCurrentLocation)
        updateModelFunction(model)
        onBack?()
    }

    func showMyLocation() {
        locationService.startUpdatingLocation()
    }

// MARK: - LocationSearchBarViewDelegate
    func textDidChanged(value: String) {
        guard !value.isEmpty else {
            view?.setTableViewHeightWithCount(of: 0)
            return
        }
        interactor?.search(query: value)
    }
}

// MARK: - SelectLocationTableDirectorDelegate
extension CreateReportSelectLocationPresenter: CreateReportSelectLocationTableDirectorDelegate {
    func didSelect(at index: Int) {
        interactor?.getCoordinatesFor(index: index,
                                      result: { [weak self] (title, coordinates) in
            guard let self = self else { return }
            guard let coordinates = coordinates else { return }
            self.address = title ?? ""
            self.location = coordinates
            self.view?.setTableViewHeightWithCount(of: 0)
            self.view?.setMapIn(point: coordinates)
            self.view?.setSearchTitleWith(text: title)
        })
    }
}

// MARK: - CreateReportSelectLocationInteractorOutput
extension CreateReportSelectLocationPresenter: CreateReportSelectLocationInteractorOutput {
    func search(results: [String]) {
        tableDirector.searchResults = results
        view?.setTableViewHeightWithCount(of: results.count)
    }

    func geocoding(result: String) {
        address = result
        view?.setSearchTitleWith(text: result)
    }
}
