//
//  CreateReportInteractor.swift
//  RadiusReport
//
//  Created by Eugene on 2/14/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import MapKit

protocol CreateReportSelectLocationInteractorInput: class {
    func search(query: String)
    func getCoordinatesFor(index: Int, result: @escaping ((String?, CLLocationCoordinate2D?) -> Void))
    func geocode(coordinate: CLLocationCoordinate2D)
}

protocol CreateReportSelectLocationInteractorOutput: class {
    func geocoding(result: String)
    func search(results: [String])
}

final class CreateReportSelectLocationInteractor: NSObject, CreateReportSelectLocationInteractorInput {
    weak var output: CreateReportSelectLocationInteractorOutput?
    private let searchCompleter = MKLocalSearchCompleter()

    private let maxValueOfAutoCompleteItems = 4
    private var searchResults = [MKLocalSearchCompletion]()
    private let locationService = GeolocationUseCase()

    func search(query: String) {
        if searchCompleter.delegate == nil {
            searchCompleter.delegate = self
        }
        searchCompleter.queryFragment = query
    }

    func geocode(coordinate: CLLocationCoordinate2D) {
        locationService.geocode(coordinate: coordinate) { [weak self] (addressString) in
            self?.output?.geocoding(result: addressString ?? "")
        }
    }

    func getCoordinatesFor(index: Int, result: @escaping ((String?, CLLocationCoordinate2D?) -> Void)) {
        let item = searchResults[index]
        let searchRequest = MKLocalSearch.Request(completion: item)
        let search = MKLocalSearch(request: searchRequest)
        search.start { (response, _) in
            let placemark = response?.mapItems.first?.placemark
            let coordinate = placemark?.coordinate
            let title = placemark?.title
            result(title, coordinate)
        }
    }
}

// MARK: - MKLocalSearchCompleterDelegate
extension CreateReportSelectLocationInteractor: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = Array(completer.results.prefix(maxValueOfAutoCompleteItems))
        let strArray = searchResults.map({ $0.title })
        output?.search(results: strArray)
    }

    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        print("MKLocalSearchCompleter error = \(error.localizedDescription)")
    }
}
