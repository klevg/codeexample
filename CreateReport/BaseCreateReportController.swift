//
//  BaseCreateReportController.swift
//  RadiusReport
//
//  Created by Eugene on 2/19/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import JGProgressHUD

protocol IBaseCreateReportController: Loadable {
    func setTitle(with string: String)
    func updateLocationTitle(with text: String?)
    func showErrorAlert()
}

class BaseCreateReportController: UIViewController {
    var hud: JGProgressHUD?

    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .clear
        return scrollView
    }()

    lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var addressView: CreateReportAddressView = {
        let view = CreateReportAddressView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var headlineView: CreateReportHeadlineView = {
        let view = CreateReportHeadlineView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setButtons()
        navigationController?.navigationBar.titleTextAttributes = [.font: UIFont.semiboldFont(ofSize: 24)]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        KeyboardTracker.shared.subscribe(delegate: self)
    }

    override func loadView() {
        super.loadView()

        let guide = self.view.safeAreaLayoutGuide
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
            scrollView.topAnchor.constraint(equalTo: guide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: guide.trailingAnchor)
        ])
        scrollView.addSubview(contentView)

        contentView.pinToBounds(scrollView)
        contentView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        let height = contentView.heightAnchor.constraint(equalTo: guide.heightAnchor, multiplier: 1)
        height.priority = .defaultLow
        height.isActive = true

        contentView.addSubview(addressView)
        contentView.addSubview(headlineView)

        NSLayoutConstraint.activate([
            addressView.heightAnchor.constraint(equalToConstant: CreateReportAddressView.height),
            addressView.topAnchor.constraint(equalTo: contentView.topAnchor),
            addressView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            addressView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            headlineView.heightAnchor.constraint(equalToConstant: CreateReportHeadlineView.height),
            headlineView.topAnchor.constraint(equalTo: addressView.bottomAnchor),
            headlineView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            headlineView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
    }

    private func setButtons() {
        let leftBarButton = UIBarButtonItem(image: UIImage.imageFrom(key: .closeBarButton)?.withRenderingMode(.alwaysOriginal),
                                            style: .plain,
                                            target: self,
                                            action: #selector(leftBarButtonTapped))
        navigationItem.leftBarButtonItem = leftBarButton
        let rightBarButton = UIBarButtonItem(image: UIImage.imageFrom(key: .arrowRightCircleFill)?.withRenderingMode(.alwaysOriginal),
                                             style: .plain,
                                             target: self,
                                             action: #selector(rightBarButtonTapped))
        navigationItem.rightBarButtonItem = rightBarButton
    }

    @objc func leftBarButtonTapped() {
        fatalError("must be overriden")
    }

    @objc func rightBarButtonTapped() {
        fatalError("must be overriden")
    }

    func setTitle(with string: String) {
        title = string
    }

    func updateLocationTitle(with text: String?) {
        addressView.update(with: text)
    }

    func showErrorAlert() {
        let alert = UIAlertController(title: "Fields are filled incorrectly",
                                      message: "please check the correctness of the completed fields",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - ICreateReportWithLinkController
extension BaseCreateReportController: KeyboardTrackerDelegate {
    func willShowKeyboard(height: CGFloat) {
        scrollView.contentInset.bottom = height
    }

    func willHideKeyboard(height: CGFloat) {
        scrollView.contentInset = .zero
    }
}
