//
//  CreateReportCoordinator.swift
//  Development
//
//  Created by Eugene on 2/26/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

final class CreateReportCoordinator: BaseFlowCoordinator, FlowFinishBehaviour {
    weak var view: UIViewController?
    weak var navigationController: UINavigationController?
    let reportRepository: ReportRepository
    let replyRepository: ReplyRepository
    let type: ActivityType
    let report: UserReportInfo?
    let replyId: Int?
    var onFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(view: UIViewController,
         reportRepository: ReportRepository,
         replyRepository: ReplyRepository,
         type: ActivityType,
         report: UserReportInfo? = nil,
         replyId: Int? = nil) {
        self.view = view
        self.reportRepository = reportRepository
        self.replyRepository = replyRepository
        self.type = type
        self.report = report
        self.replyId = replyId
    }

    // MARK: - Flow
    override func start() {
        type == .report ? showCreateReportWithCamera() : showCreateReplyWithCamera()
    }

    private func showCreateReportWithCamera() {
        let module = CreateReportCameraAssembly.build()
        let nav = UINavigationController(rootViewController: module.view)
        navigationController = nav

        module.presenter.onCancel = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.onFinishFlow?()
        }
        module.presenter.onLink = { [weak self] in
            self?.showCreateReportWithLink()
        }
        module.presenter.onText = { [weak self] in
            self?.showCreateReportWithText()
        }
        module.presenter.onNext = { [weak self] (image, url) in
            self?.showCreateReportWithMedia(reportImage: image, movieURL: url)
        }
        navigationController?.modalPresentationStyle = .fullScreen
        navigationController?.navigationBar.barTintColor = .white
        view?.present(nav, animated: true)
    }
    
    private func showCreateReplyWithCamera() {
        let module = CreateReportCameraAssembly.build()
        let nav = UINavigationController(rootViewController: module.view)
        navigationController = nav

        module.presenter.onCancel = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.onFinishFlow?()
        }
        module.presenter.onLink = { [weak self] in
            self?.showCreateReplyWithLink()
        }
        module.presenter.onText = { [weak self] in
            self?.showCreateReplyWithText()
        }
        module.presenter.onNext = { [weak self] (image, url) in
            self?.showCreateReplyWithMedia(reportImage: image, movieURL: url)
        }
        navigationController?.modalPresentationStyle = .fullScreen
        navigationController?.navigationBar.barTintColor = .white
        view?.present(nav, animated: true)
    }
    

    private func showCreateReportWithLink() {
        let module = CreateReportWithLinkAssembly.build(reportRepository: reportRepository)

        module.presenter.onCancel = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.onFinishFlow?()
        }
        module.presenter.onLocation = { [weak self] function in
            self?.showCreateReportSelectLocation(updateModelFunction: function)
        }
        module.presenter.onSelect = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.showCreateReportCompletionView(callback: {
                self?.onFinishFlow?()
            })
        }
        navigationController?.pushViewController(module.view, animated: true)
    }

    private func showCreateReportWithText() {
        let module = CreateReportWithTextAssembly.build(reportRepository: reportRepository)

        module.presenter.onCancel = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.onFinishFlow?()
        }
        module.presenter.onLocation = { [weak self] function in
            self?.showCreateReportSelectLocation(updateModelFunction: function)
        }
        module.presenter.onSelect = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.showCreateReportCompletionView(callback: {
                self?.onFinishFlow?()
            })
        }
        navigationController?.pushViewController(module.view, animated: true)
    }

    private func showCreateReportWithMedia(reportImage: UIImage?, movieURL: URL?) {
        let module = CreateReportWithMediaAssembly.build(reportImage: reportImage,
                                                         movieURL: movieURL,
                                                         reportRepository: reportRepository)
        module.presenter.onCancel = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.onFinishFlow?()
        }
        module.presenter.onLocation = { [weak self] function in
            self?.showCreateReportSelectLocation(updateModelFunction: function)
        }
        module.presenter.onSelect = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.showCreateReportCompletionView(callback: {
                self?.onFinishFlow?()
            })
        }
        navigationController?.pushViewController(module.view, animated: true)
    }

    private func showCreateReportSelectLocation(updateModelFunction: @escaping (BaseCreateReportPresenter.LocationModel) -> Void) {
         let module = CreateReportSelectLocationAssembly.build(updateModelFunction: updateModelFunction)
         module.presenter.onBack = { [weak self] in
             self?.navigationController?.popViewController(animated: true)
         }
        navigationController?.pushViewController(module.view, animated: true)
     }

     private func showCreateReportCompletionView(callback: @escaping (() -> Void)) {
        guard let tabbarVC = view as? UITabBarController else { return }
        CompletionView.show(on: tabbarVC, completion: callback)
     }

    // MARK: - Reply Coordinator
    private func showCreateReplyWithMedia(reportImage: UIImage?, movieURL: URL?) {
//        let module = CreateReportWithMediaAssembly.build(reportImage: reportImage,
//                                                         movieURL: movieURL,
//                                                         reportRepository: reportRepository)
//        module.presenter.onCancel = { [weak self] in
//            self?.navigationController?.dismiss(animated: true)
//            self?.onFinishFlow?()
//        }
//        module.presenter.onLocation = { [weak self] function in
//            self?.showCreateReportSelectLocation(updateModelFunction: function)
//        }
//        module.presenter.onSelect = { [weak self] in
//            self?.navigationController?.dismiss(animated: true)
//            self?.showCreateReportCompletionView(callback: {
//                self?.onFinishFlow?()
//            })
//        }
//        navigationController?.pushViewController(module.view, animated: true)
    }

    private func showCreateReplyWithLink() {
        let module = CreateReplyWithLinkAssembly.build(replyRepository: replyRepository, report: report, replyId: replyId)

        module.presenter.onCancel = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.onFinishFlow?()
        }

        module.presenter.onSelect = { [weak self] in
            self?.navigationController?.dismiss(animated: true)
            self?.showCreateReportCompletionView(callback: {
                self?.onFinishFlow?()
            })
        }
        navigationController?.pushViewController(module.view, animated: true)
    }

    private func showCreateReplyWithText() {
//        let module = CreateReportWithTextAssembly.build(reportRepository: reportRepository)
//
//        module.presenter.onCancel = { [weak self] in
//            self?.navigationController?.dismiss(animated: true)
//            self?.onFinishFlow?()
//        }
//        module.presenter.onLocation = { [weak self] function in
//            self?.showCreateReportSelectLocation(updateModelFunction: function)
//        }
//        module.presenter.onSelect = { [weak self] in
//            self?.navigationController?.dismiss(animated: true)
//            self?.showCreateReportCompletionView(callback: {
//                self?.onFinishFlow?()
//            })
//        }
//        navigationController?.pushViewController(module.view, animated: true)
    }
    
}
