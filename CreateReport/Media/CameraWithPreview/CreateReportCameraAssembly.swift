//
//  CreateReportCameraAssembly.swift
//  RadiusReport
//
//  Created by Eugene on 2/26/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

final class CreateReportCameraAssembly {
    struct Module {
        let view: CreateReportCameraViewController
        let presenter: CreateReportCameraPresenter
    }

    private init() {}

    static func build() -> Module {
        let view = CreateReportCameraViewController()
        let presenter = CreateReportCameraPresenter(view: view)
        view.presenter = presenter
        return .init(view: view, presenter: presenter)
    }
}
