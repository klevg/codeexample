//
//  CreateReportImagePreview.swift
//  RadiusReport
//
//  Created by Eugene on 2/26/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit

final class CreateReportImagePreview: XibView {
    private let cornerRadius: CGFloat = 20

    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var previewControls: CreateReportPreviewControls!

    func setup(delegate: CreateReportPreviewControlsDelegate) {
        backgroundColor = .radiusPureBlack
        previewControls.delegate = delegate
    }

    func setup(with image: UIImage?) {
        iconImageView.image = image
        iconImageView.contentMode = .scaleAspectFill
    }

    func setupCornerRadius() {
        iconImageView.layer.cornerRadius = cornerRadius
        iconImageView.clipsToBounds = true
        iconImageView.layer.masksToBounds = true
    }
}
