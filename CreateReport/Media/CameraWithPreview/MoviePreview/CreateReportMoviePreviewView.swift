//
//  MoviePreviewView.swift
//  RadiusReport
//
//  Created by Евгений Клебан on 3/2/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import AVFoundation
import Foundation
import AVKit

final class CreateReportMoviePreviewView: XibView {
    private let cornerRadius: CGFloat = 20

    private var playerLayer: AVPlayerLayer?
    private var player: AVPlayer?
    private var isMoviePlaying = false

    @IBOutlet private weak var moviePreview: UIView!
    @IBOutlet private weak var playLabel: UILabel!

    private func playMovie() {
        if !isMoviePlaying {
            isMoviePlaying = true
            playLabel.isHidden = true
            play()
        } else {
            isMoviePlaying = false
            playLabel.isHidden = false
            stop()
        }
    }

    func configure(videoURL: URL) {
        backgroundColor = .radiusPureBlack
        player = AVPlayer(url: videoURL)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = moviePreview.bounds
        playerLayer?.videoGravity = AVLayerVideoGravity.resize
        if let playerLayer = self.playerLayer {
            moviePreview.layer.addSublayer(playerLayer)
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachTheEndOfTheVideo(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: self.player?.currentItem)
    }

    func setupCornerRadius() {
        moviePreview.layer.cornerRadius = cornerRadius
        moviePreview.clipsToBounds = true
    }

    private func play() {
        if player?.timeControlStatus != .playing {
            player?.play()
        }
    }

    private func pause() {
        player?.pause()
    }

    private func stop() {
        player?.pause()
        player?.seek(to: CMTime.zero)
    }

    @objc private func reachTheEndOfTheVideo(_ notification: Notification) {
        isMoviePlaying = false
        playLabel.isHidden = false
        stop()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        playMovie()
    }
}
