//
//  CameraFlashState.swift
//  RadiusReport
//
//  Created by Eugene on 2/28/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import AVFoundation

enum CameraFlashState {
    case on
    case off
    case auto

    var iconImage: UIImage? {
        switch self {
        case .auto:
            return .imageFrom(key: .flashAutoIcon)
        case .off:
            return .imageFrom(key: .flashOffIcon)
        case .on:
            return .imageFrom(key: .flashOnIcon)
        }
    }

    var flashMode: AVCaptureDevice.FlashMode {
        switch self {
        case .auto:
            return .auto
        case .off:
            return .off
        case .on:
            return .on
        }
    }

    var torchMode: AVCaptureDevice.TorchMode {
        switch self {
        case .auto:
            return .auto
        case .off:
            return .off
        case .on:
            return .on
        }
    }

    func nextMode() -> CameraFlashState {
        switch self {
        case .auto:
            return .on
        case .on:
            return .off
        case .off:
            return .auto
        }
    }
}
