//
//  CameraController.swift
//  RadiusReport
//
//  Created by Eugene on 2/20/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//
import AVFoundation
import UIKit

protocol ICameraController: class {
    func prepare(completion: @escaping (Error?) -> Void)
    func displayPreview(on view: UIView) throws

    func switchCameras() throws
    var flashMode: CameraFlashState { get set }

    func startSession()
    func stopSession()

    func captureImage(completion: @escaping (UIImage?, Error?) -> Void)

    func startRecording()
    func stopRecording(completion: @escaping ((URL?, Error?) -> Void))
    var outputMovieURL: URL? { get }
}

final class CameraController: NSObject {

    // MARK: Properties
    private var initialView: UIView!
    private let cameraSession: CameraSessionController

    var flashMode: CameraFlashState {
        get {
            return cameraSession.flashMode
        }
        set {
            cameraSession.flashMode = newValue
        }
    }

    var outputMovieURL: URL? {
        return cameraSession.outputURL
    }

    private var photoCaptureCompletionBlock: ((UIImage?, Error?) -> Void)?

    private lazy var focusGesture: UITapGestureRecognizer = {
        let instance = UITapGestureRecognizer(target: self, action: #selector(tapToFocus(_:)))
        instance.cancelsTouchesInView = false
        instance.numberOfTapsRequired = 1
        instance.numberOfTouchesRequired = 1
        return instance
    }()

    private lazy var focusSquareView: CameraFocusSquare = {
        let drawSquare = CameraFocusSquare(frame: CGRect(x: 0,
                                                         y: 0,
                                                         width: CameraFocusSquare.sideSize,
                                                         height: CameraFocusSquare.sideSize))
        drawSquare.backgroundColor = .clear
        drawSquare.alpha = 0
        return drawSquare
    }()

    // MARK: - Initialization
    override init() {
        self.cameraSession = CameraSessionController()
        super.init()
    }

    @objc func tapToFocus(_ gesture: UITapGestureRecognizer) {
        let touchPoint = gesture.location(in: initialView)
        cameraSession.tapToFocus(touchPoint: touchPoint)
        let focusSquareSideSize = CameraFocusSquare.sideSize
        let x = touchPoint.x - focusSquareSideSize / 2
        let y = touchPoint.y - focusSquareSideSize / 2
        focusSquareView.frame = CGRect(x: x,
                                       y: y,
                                       width: focusSquareSideSize,
                                       height: focusSquareSideSize)
        updateFocusSquare()
    }

    private func updateFocusSquare() {
        focusSquareView.alpha = 0.9

        CameraFocusSquare.animate(withDuration: 1,
                                  animations: {
            self.focusSquareView.alpha = 1
        },
                           completion: { _ in
            self.focusSquareView.alpha = 0
        })
    }
}

// MARK: - ICameraController
extension CameraController: ICameraController {
    func startSession() {
        cameraSession.startSession()
    }

    func stopSession() {
        cameraSession.stopSession()
    }

    func startRecording() {
        cameraSession.startRecording()
    }

    func stopRecording(completion: @escaping ((URL?, Error?) -> Void)) {
        cameraSession.stopRecording(completionHandler: completion)
    }

    func prepare(completion: @escaping (Error?) -> Void) {
        cameraSession.prepare(completion: completion)
    }

    func displayPreview(on view: UIView) throws {
        initialView = view
        initialView.addGestureRecognizer(focusGesture)
        initialView.addSubview(focusSquareView)

        try cameraSession.displayPreview(on: initialView)
    }

    func captureImage(completion: @escaping (UIImage?, Error?) -> Void) {
        cameraSession.captureImage(completion: completion)
    }

    func switchCameras() throws {
        try cameraSession.switchCameras()
    }
}
