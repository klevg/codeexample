//
//  FocusRect.swift
//  RadiusReport
//
//  Created by Eugene on 2/27/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

final class CameraFocusSquare: UIView {
    static let sideSize: CGFloat = 250

    override func draw(_ rect: CGRect) {
        let h = rect.height
        let w = rect.width
        let color: UIColor = .yellow

        let drect = CGRect(x: (w * 0.25),
                           y: (h * 0.25),
                           width: (w * 0.5),
                           height: (h * 0.5))
        let bpath = UIBezierPath(rect: drect)

        color.set()
        bpath.stroke()
    }
}
