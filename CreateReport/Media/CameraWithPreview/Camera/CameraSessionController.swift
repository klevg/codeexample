//
//  CameraSessionController.swift
//  RadiusReport
//
//  Created by Eugene on 2/28/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//
import AVFoundation
import Foundation

final class CameraSessionController: NSObject {
    enum Position {
        case front
        case rear
    }

    enum ControllerError: Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }

    private var captureSession: AVCaptureSession?
    private var currentCameraPosition: Position?

    private var frontCamera: AVCaptureDevice?
    private var frontCameraInput: AVCaptureDeviceInput?

    private var photoOutput: AVCapturePhotoOutput?

    private var rearCamera: AVCaptureDevice?
    private var rearCameraInput: AVCaptureDeviceInput?

    private var previewLayer: AVCaptureVideoPreviewLayer?

    var flashMode = CameraFlashState.auto
    private var photoCaptureCompletionBlock: ((UIImage?, Error?) -> Void)?

    private let movieOutput = AVCaptureMovieFileOutput()

    var outputURL: URL?
    private var movieCaptureCompletionBlock: ((URL?, Error?) -> Void)?

    func prepare(completion: @escaping (Error?) -> Void) {
        DispatchQueue(label: "prepare").async { [weak self] in
            do {
                guard let self = self else { return }
                self.createCaptureSession()
                try self.configureCaptureDevices()
                try self.configureDeviceInputs()
                try self.setupMovieSession()
                try self.configurePhotoOutput()
                self.startSession()
                DispatchQueue.main.async {
                    completion(nil)
                }
            } catch let error {
                DispatchQueue.main.async {
                    completion(error)
                }
            }
        }
    }

    func displayPreview(on view: UIView) throws {
        guard let captureSession = captureSession, captureSession.isRunning else { throw ControllerError.captureSessionIsMissing }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer?.connection?.videoOrientation = .portrait

        view.layer.insertSublayer(previewLayer!, at: 0)
        previewLayer?.frame = view.frame
    }

    func switchCameras() throws {
        guard let currentCameraPosition = currentCameraPosition,
            let captureSession = captureSession, captureSession.isRunning else { throw ControllerError.captureSessionIsMissing }

        captureSession.beginConfiguration()

        switch currentCameraPosition {
        case .front:
            try switchToRearCamera(captureSession: captureSession)
        case .rear:
            try switchToFrontCamera(captureSession: captureSession)
        }
        captureSession.commitConfiguration()
    }

    func captureImage(completion: @escaping (UIImage?, Error?) -> Void) {
        guard let captureSession = captureSession,
          captureSession.isRunning else { completion(nil, ControllerError.captureSessionIsMissing); return }

        let settings = AVCapturePhotoSettings()
        settings.flashMode = flashMode.flashMode

        photoOutput?.capturePhoto(with: settings, delegate: self)
        photoCaptureCompletionBlock = completion
    }

    func tapToFocus(touchPoint: CGPoint) {
        guard let previewLayer = previewLayer else {
            print("Expected a previewLayer")
            return
        }
        guard let device = getCurrentDevice() else {
            print("Expected a device")
            return
        }

        let convertedPoint: CGPoint = previewLayer.captureDevicePointConverted(fromLayerPoint: touchPoint)
        if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(AVCaptureDevice.FocusMode.autoFocus) {
            do {
                try device.lockForConfiguration()
                device.focusPointOfInterest = convertedPoint
                device.focusMode = AVCaptureDevice.FocusMode.autoFocus
                device.unlockForConfiguration()
            } catch {
                print("unable to focus")
            }
        }
    }

    private func getCurrentDevice() -> AVCaptureDevice? {
        var currentDevice: AVCaptureDevice?
        switch currentCameraPosition {
        case .front:
            currentDevice = frontCamera
        default:
            currentDevice = rearCamera
        }
        return currentDevice
    }

    // MARK: - Camera Session
    func startSession() {
        guard let captureSession = captureSession else { return }
        if !captureSession.isRunning {
            DispatchQueue.main.async {
                self.captureSession!.startRunning()
            }
        }
    }

    func stopSession() {
        guard let captureSession = captureSession else { return }
        if captureSession.isRunning {
            DispatchQueue.main.async {
                self.captureSession!.stopRunning()
            }
        }
    }

    func tempURL() -> URL? {
        let directory = NSTemporaryDirectory() as NSString
        guard directory != "" else { return nil }
        let path = directory.appendingPathComponent(NSUUID().uuidString + ".mov")
        return URL(fileURLWithPath: path)
    }

    func startRecording() {
        guard !movieOutput.isRecording else { return }
        setTorchMode(flashMode.torchMode)
        outputURL = tempURL()
        movieOutput.startRecording(to: outputURL!, recordingDelegate: self)
    }

    func stopRecording(completionHandler: @escaping (URL?, Error?) -> Void) {
        if movieOutput.isRecording == true {
            if flashMode == .on {
                setTorchMode(.off)
            }
            movieOutput.stopRecording()
        }
        movieCaptureCompletionBlock = completionHandler
    }

    private func setTorchMode(_ torchMode: AVCaptureDevice.TorchMode) {
        guard let device = getCurrentDevice() else { return }
        if device.isTorchModeSupported(torchMode) && device.torchMode != torchMode {
            do {
                try device.lockForConfiguration()
                device.torchMode = torchMode
                device.unlockForConfiguration()
            } catch {
                print("Error: \(error)")
            }
        }
    }

}

// MARK: - AVCapturePhotoCaptureDelegate
extension CameraSessionController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput,
                     didFinishProcessingPhoto photo: AVCapturePhoto,
                     error: Error?) {
        if let error = error {
            photoCaptureCompletionBlock?(nil, error)
        } else if let imageData = photo.fileDataRepresentation(),
          let photo = UIImage(data: imageData) {
            photoCaptureCompletionBlock?(photo, nil)
        } else {
            photoCaptureCompletionBlock?(nil, ControllerError.unknown)
        }
    }
}

// MARK: - AVCaptureFileOutputRecordingDelegate
extension CameraSessionController: AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput,
                    didFinishRecordingTo outputFileURL: URL,
                    from connections: [AVCaptureConnection],
                    error: Error?) {
        if let error = error {
            movieCaptureCompletionBlock?(nil, error)
        } else {
            movieCaptureCompletionBlock?(outputURL, nil)
        }
    }
}

// MARK: - Camera preparation
private extension CameraSessionController {
    func createCaptureSession() {
        captureSession = AVCaptureSession()
    }

    func configureCaptureDevices() throws {

        let session = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera],
                                                       mediaType: AVMediaType.video,
                                                       position: .unspecified)
        let cameras = session.devices.compactMap { $0 }
        guard !cameras.isEmpty else { throw ControllerError.noCamerasAvailable }

        for camera in cameras {
            if camera.position == .front {
                frontCamera = camera
            }

            if camera.position == .back {
                rearCamera = camera

                try camera.lockForConfiguration()
                camera.focusMode = .continuousAutoFocus
                camera.unlockForConfiguration()
            }
        }
    }

    func configureDeviceInputs() throws {
        guard let captureSession = captureSession else { throw ControllerError.captureSessionIsMissing }
        if let rearCamera = rearCamera {
            rearCameraInput = try AVCaptureDeviceInput(device: rearCamera)

            if captureSession.canAddInput(rearCameraInput!) { captureSession.addInput(rearCameraInput!) }

            currentCameraPosition = .rear
        } else if let frontCamera = self.frontCamera {
            frontCameraInput = try AVCaptureDeviceInput(device: frontCamera)

            if captureSession.canAddInput(frontCameraInput!) {
                captureSession.addInput(frontCameraInput!)
            } else {
                throw ControllerError.inputsAreInvalid
            }
            currentCameraPosition = .front
        } else {
            throw ControllerError.noCamerasAvailable
        }
    }

    func configurePhotoOutput() throws {
        guard let captureSession = captureSession else { throw ControllerError.captureSessionIsMissing }

        photoOutput = AVCapturePhotoOutput()
        photoOutput!.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])],
                                                   completionHandler: nil)

        if captureSession.canAddOutput(photoOutput!) {
            captureSession.addOutput(photoOutput!)
        }
    }

    func setupMovieSession() throws {
        guard let captureSession = captureSession else { throw ControllerError.captureSessionIsMissing }
        captureSession.sessionPreset = AVCaptureSession.Preset.high
        if captureSession.canAddOutput(movieOutput) {
            captureSession.addOutput(movieOutput)
        }

        let connection = movieOutput.connection(with: AVMediaType.video)

        if (connection?.isVideoOrientationSupported)! {
            connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        }

        if (connection?.isVideoStabilizationSupported)! {
            connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
        }

        guard let device = getCurrentDevice() else { return }

        if device.isSmoothAutoFocusSupported {
            do {
                try device.lockForConfiguration()
                device.isSmoothAutoFocusEnabled = false
                device.unlockForConfiguration()
            } catch {
                throw ControllerError.captureSessionIsMissing
            }
        }
        // Setup Microphone
        let microphone = AVCaptureDevice.default(for: AVMediaType.audio)!
        do {
            let micInput = try AVCaptureDeviceInput(device: microphone)
            if captureSession.canAddInput(micInput) {
                captureSession.addInput(micInput)
            }
        } catch {
            throw ControllerError.captureSessionIsMissing
        }
    }
}

// MARK: - SwitchCamera
private extension CameraSessionController {
    func switchToFrontCamera(captureSession: AVCaptureSession) throws {
        guard let rearCameraInput = rearCameraInput, captureSession.inputs.contains(rearCameraInput),
            let frontCamera = frontCamera else { throw ControllerError.invalidOperation }

        frontCameraInput = try AVCaptureDeviceInput(device: frontCamera)

        captureSession.removeInput(rearCameraInput)

        if captureSession.canAddInput(frontCameraInput!) {
            captureSession.addInput(frontCameraInput!)
            self.currentCameraPosition = .front
        } else {
            throw ControllerError.invalidOperation
        }
    }

    func switchToRearCamera(captureSession: AVCaptureSession) throws {
        guard let frontCameraInput = frontCameraInput,
            captureSession.inputs.contains(frontCameraInput),
            let rearCamera = rearCamera else { throw ControllerError.invalidOperation }

        rearCameraInput = try AVCaptureDeviceInput(device: rearCamera)

        captureSession.removeInput(frontCameraInput)

        if captureSession.canAddInput(rearCameraInput!) {
            captureSession.addInput(rearCameraInput!)

            self.currentCameraPosition = .rear
        } else {
            throw ControllerError.invalidOperation
        }
    }
}
