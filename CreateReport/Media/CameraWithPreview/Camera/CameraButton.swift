//
//  CameraButton.swift
//  RadiusReport
//
//  Created by Eugene on 2/24/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit

protocol CameraButtonDelegate: class {
    func didTap()
    func didStartLongPress()
    func didFinishLongPress()
}

final class CameraButton: UIButton {
    // MARK: - Constants
    private struct Constants {
        static let radius: CGFloat = 62
        static let radiusForVideo: CGFloat = 65
        static let indicatorWidth: CGFloat = 3
        static let separatorWith: CGFloat = 4
    }

    private var activeColor: UIColor = .radiusRed
    private var inactiveColor: UIColor = .lightGrey

    weak var delegate: CameraButtonDelegate?

    private let separatorShape = CAShapeLayer()
    private let progressShape = CAShapeLayer()
    private let backgroundShape = CAShapeLayer()

    private var isAnimationRunning = false

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        setupLayout()

        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector (buttonTapped))
        let longGesture = UILongPressGestureRecognizer(target: self,
                                                       action: #selector(longPressed))

        tapGesture.numberOfTapsRequired = 1
        longGesture.minimumPressDuration = 0.25
        longGesture.cancelsTouchesInView = false
        addGestureRecognizer(tapGesture)
        addGestureRecognizer(longGesture)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = Constants.radiusForVideo / 2
    }

    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? activeColor : inactiveColor
            if isAnimationRunning, !isHighlighted {
                finishAnimation()
            }
        }
    }

    // MARK: - Actions
    @objc private func buttonTapped() {
        delegate?.didTap()
    }

    @objc private func longPressed() {
        if !isAnimationRunning {
            delegate?.didStartLongPress()
            isAnimationRunning = true
            startAnimation()
        }
    }
}

// MARK: - SetupLayout
private extension CameraButton {
    func setupLayout() {
        backgroundColor = inactiveColor

        layer.addSublayer(separatorShape)
        let boundsCenter = CGPoint(x: bounds.maxX / 2,
                                   y: bounds.maxY / 2)
        separatorShape.position = boundsCenter
        separatorShape.path = UIBezierPath(arcCenter: .zero,
                                           radius: Constants.radius / 2,
                                           startAngle: 0,
                                           endAngle: 2 * CGFloat.pi,
                                           clockwise: true).cgPath
        separatorShape.strokeColor = UIColor.black.cgColor
        separatorShape.lineWidth = Constants.separatorWith
        separatorShape.fillColor = UIColor.clear.cgColor

        layer.addSublayer(backgroundShape)
        layer.addSublayer(progressShape)

        backgroundShape.position = boundsCenter
        backgroundShape.path = UIBezierPath(arcCenter: .zero,
                                            radius: Constants.radiusForVideo / 2,
                                            startAngle: CGFloat.pi * 3/2,
                                            endAngle: CGFloat.pi * 7/2 ,
                                            clockwise: true).cgPath
        backgroundShape.strokeColor = inactiveColor.cgColor
        backgroundShape.lineWidth = Constants.indicatorWidth
        backgroundShape.fillColor = UIColor.clear.cgColor
        progressShape.path = backgroundShape.path
        progressShape.position = backgroundShape.position
        progressShape.strokeColor = activeColor.cgColor
        progressShape.strokeEnd = 0
        progressShape.lineWidth = backgroundShape.lineWidth
        progressShape.fillColor = UIColor.clear.cgColor
    }
}

// MARK: - Animation
private extension CameraButton {
    func startAnimation() {
        let animation = CABasicAnimation(keyPath: #keyPath(CAShapeLayer.strokeEnd))
        animation.fromValue = progressShape.strokeEnd
        animation.duration = CreateReportSettings.videoDuration
        animation.toValue = 1
        animation.timingFunction = CAMediaTimingFunction(name: .linear)
        animation.delegate = self
        progressShape.add(animation, forKey: nil)
    }

    func finishAnimation() {
        delegate?.didFinishLongPress()
        isAnimationRunning = false
        progressShape.removeAllAnimations()
        progressShape.strokeEnd = 0
    }
}

// MARK: - CAAnimationDelegate
extension CameraButton: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            isHighlighted = false
            finishAnimation()
        }
    }
}
