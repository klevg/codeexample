//
//  CreateReportPreviewControls.swift
//  RadiusReport
//
//  Created by Eugene on 3/4/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

protocol CreateReportPreviewControlsDelegate: class {
    func didTapOnCloseButton()
    func didTapOnCancelButton()
    func didTapOnNextButton()
}

final class CreateReportPreviewControls: XibView {
    weak var delegate: CreateReportPreviewControlsDelegate?

    @IBAction private func closeButtonTapped() {
        delegate?.didTapOnCloseButton()
    }

    @IBAction private func cancelButtonTapped() {
        delegate?.didTapOnCancelButton()
    }

    @IBAction private func nextButtonTapped() {
        delegate?.didTapOnNextButton()
    }
}
