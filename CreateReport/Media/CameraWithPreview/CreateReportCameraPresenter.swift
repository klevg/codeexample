//
//  CreateReportCameraPresenter.swift
//  RadiusReport
//
//  Created by Eugene on 2/26/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import Photos

protocol ICreateReportCameraPresenter: ViewLifecycleSupportable,
                                       CreateReportPreviewControlsDelegate,
                                       CreateReportCameraViewDelegate,
                                       CreateReportNoCameraAccessViewDelegate {
    func setImageFromLibrary(_ image: UIImage)
    func setUrlFromLibrary(_ url: URL)
    func startCameraSession()
    func stopCameraSession()
    var onCancel: (() -> Void)? { get set }
    var onText: (() -> Void)? { get set }
    var onLink: (() -> Void)? { get set }
}

final class CreateReportCameraPresenter {

    // MARK: - Flow Control
    var onCancel: (() -> Void)?
    var onText: (() -> Void)?
    var onLink: (() -> Void)?
    var onNext: ((UIImage?, URL?) -> Void)?

    // MARK: - Properties
    private var reportImage: UIImage?
    private var movieURL: URL?
    private var isMediaFromLibrary = false

    private weak var view: ICreateReportCameraViewController?
    private let cameraController: ICameraController

    // MARK: - Initialization
    init(view: ICreateReportCameraViewController) {
        self.view = view
        self.cameraController = CameraController()
    }

    // MARK: - Lifecycle
    func viewDidLoad() {
        let locationService = GeolocationUseCase()
        locationService.requestWhenInUseAuthorization()
        configureCameraController()
    }

    func viewWillDisappear() {
        stopCameraSession()
    }

    private func configureCameraController() {
        cameraController.prepare { [weak self] error in
            if error != nil {
                self?.view?.show(state: .noAccess)
                self?.view?.showAlertForSettings()
                return
            }
            guard let view = self?.view?.getCapturePreviewView() else { return }
            try? self?.cameraController.displayPreview(on: view)
            self?.view?.show(state: .camera)
        }
    }
}

// MARK: - ICreateReportCameraPresenter
extension CreateReportCameraPresenter: ICreateReportCameraPresenter {
    func startCameraSession() {
        cameraController.startSession()
    }

    func stopCameraSession() {
        cameraController.stopSession()
    }

    func setImageFromLibrary(_ image: UIImage) {
        isMediaFromLibrary = true
        reportImage = image
        view?.show(state: .imagePreview(image: image))
    }

    func setUrlFromLibrary(_ url: URL) {
        isMediaFromLibrary = true
        movieURL = url
        view?.show(state: .moviePreview(path: url))
    }
}

// MARK: - CreateReportCameraViewDelegate
extension CreateReportCameraPresenter: CreateReportCameraViewDelegate {
    func didTapOnMediaButton() {
        view?.showPhotoLibrary()
    }

    func didStartLongPress() {
        cameraController.startRecording()
    }

    func didFinishLongPress() {
        cameraController.stopRecording { [weak self] (url, error) in
            if let error = error {
                print(error)
            } else if let url = url {
                self?.movieURL = url
                self?.view?.show(state: .moviePreview(path: url))
            }
        }
    }

    func didTapSwitchCamera() {
        do {
            try cameraController.switchCameras()
        } catch {
            print(error)
        }
    }

    func didTapOnToggleFlash() {
        let nextMode = cameraController.flashMode.nextMode()
        cameraController.flashMode = nextMode
        view?.setupFlashButtonWith(image: nextMode.iconImage)
    }

    func didCapture() {
        cameraController.captureImage { [weak self] (image, _) in
            guard let image = image else { return }
            self?.reportImage = image
            self?.isMediaFromLibrary = false
            self?.view?.show(state: .imagePreview(image: image))
        }
    }

    func didTapOnText() {
        onText?()
    }

    func didTapOnLink() {
        onLink?()
    }

    func didTapOnCloseButton() {
        onCancel?()
    }
}

// MARK: - CreateReportImagePreviewDelegate
extension CreateReportCameraPresenter: CreateReportPreviewControlsDelegate {
    func didTapOnCancelButton() {
        reportImage = nil
        movieURL = nil
        isMediaFromLibrary = false
        view?.show(state: .camera)
    }

    func didTapOnNextButton() {
        if !isMediaFromLibrary {
            if let reportImage = reportImage {
                try? PHPhotoLibrary.shared().performChangesAndWait {
                    PHAssetChangeRequest.creationRequestForAsset(from: reportImage)
                }
            } else if let movieURL = movieURL {
                try? PHPhotoLibrary.shared().performChangesAndWait {
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: movieURL)
                }
            }
        }
        onNext?(reportImage, movieURL)
    }
}

// MARK: - CreateReportNoCameraAccessViewDelegate
extension CreateReportCameraPresenter: CreateReportNoCameraAccessViewDelegate {
    func didTapOnEnableCameraButton() {
        configureCameraController()
    }
}
