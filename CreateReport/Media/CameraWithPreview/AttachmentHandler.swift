//
//  AttachmentHandler.swift
//  RadiusReport
//
//  Created by Eugene on 3/3/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos

final class AttachmentHandler: NSObject {
    static let shared = AttachmentHandler()
    fileprivate var currentVC: UIViewController?

    // MARK: - Internal Properties
    var imagePickedBlock: ((UIImage) -> Void)?
    var videoPickedBlock: ((URL) -> Void)?

    // MARK: - Constants
    struct Constants {
        static let settingsBtnTitle = "Settings"
        static let cancelBtnTitle = "Cancel"
        static let alertForPhotoLibraryMessage = """
        App does not have access to your photos and video.
        To enable access, tap settings and turn on Photo Library Access.
        """
    }

    // MARK: - Initialization
    private override init() {
        super.init()
    }

    // MARK: - ShowAttachmentActionSheet
    func showAttachmentActionSheet(vc: UIViewController) {
        currentVC = vc
        authorisationStatus(vc: self.currentVC!)
    }

    // MARK: - Authorisation Status
    func authorisationStatus(vc: UIViewController) {
        currentVC = vc
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            photoLibrary()
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ [weak self] (status) in
                guard let self = self else { return }
                if status == PHAuthorizationStatus.authorized {
                    print("access given")
                    self.photoLibrary()
                } else {
                    print("restriced manually")
                    self.addAlertForSettings()
                }
            })
        case .denied:
            print("permission denied")
            self.addAlertForSettings()
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings()
        default:
            break
        }
    }

    // MARK: - PHOTO PICKER
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = ["public.image", "public.movie"]
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }

    // MARK: - SETTINGS ALERT
    func addAlertForSettings() {
        let alertTitle = Constants.alertForPhotoLibraryMessage

        let cameraUnavailableAlertController = UIAlertController(title: alertTitle,
                                                                 message: nil,
                                                                 preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle,
                                           style: .destructive) { _ in
            let settingsUrl = URL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController, animated: true, completion: nil)
    }
}

// MARK: - IMAGE PICKER DELEGATE
extension AttachmentHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.imagePickedBlock?(image)
        } else if let videoUrl = info[.mediaURL] as? URL,
            let data = try? Data(contentsOf: videoUrl) {
            print("videourl: \(videoUrl); File size before compression: \(Double(data.count / 1048576)) mb")
            compressWithSessionStatusFunc(videoUrl)
        }
        currentVC?.dismiss(animated: true, completion: nil)
    }

    // MARK: Video Compressing technique
    private func compressWithSessionStatusFunc(_ videoUrl: URL) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }

            switch session.status {
            case .completed:
                guard let compressedData = try? Data(contentsOf: compressedURL) else {
                    return
                }
                print("File size after compression: \(Double(compressedData.count / 1048576)) mb")
                DispatchQueue.main.async {
                    self.videoPickedBlock?(compressedURL)
                }
            default: break
            }
        }
    }

    // Now compression is happening with medium quality, we can change when ever it is needed
    private func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?) -> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously {
            handler(exportSession)
        }
    }
}
