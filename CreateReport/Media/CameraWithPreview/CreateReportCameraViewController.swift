//
//  CreateReportCameraViewController.swift
//  RadiusReport
//
//  Created by Eugene on 2/20/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import UIKit
import Photos
import JGProgressHUD

protocol ICreateReportCameraViewController: Loadable {
    func getCapturePreviewView() -> UIView
    func setupFlashButtonWith(image: UIImage?)
    func showPhotoLibrary()
    func show(state: CreateReportCameraViewController.State)
    func showAlertForSettings()
}

final class CreateReportCameraViewController: UIViewController {
    enum State {
        case camera
        case imagePreview(image: UIImage)
        case moviePreview(path: URL)
        case noAccess
    }

    var presenter: ICreateReportCameraPresenter!
    private var imagePicker = UIImagePickerController()
    var hud: JGProgressHUD?

    // MARK: Views
    private lazy var capturePreviewView: CreateReportCameraView = {
        let view = CreateReportCameraView()
        view.delegate = presenter
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var imagePreview: CreateReportImagePreview = {
        let view = CreateReportImagePreview()
        view.setup(delegate: presenter)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()

    private lazy var moviePreview: CreateReportMoviePreviewView = {
        let preview = CreateReportMoviePreviewView()
        preview.translatesAutoresizingMaskIntoConstraints = false
        preview.isHidden = true

        let controls = CreateReportPreviewControls()
        controls.translatesAutoresizingMaskIntoConstraints = false
        preview.addSubview(controls)
        preview.pinToBounds(controls)
        controls.delegate = presenter

        return preview
    }()

    private lazy var noCameraAccessView: CreateReportNoCameraAccessView = {
        let view = CreateReportNoCameraAccessView()
        view.delegate = presenter
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()

    override var prefersStatusBarHidden: Bool { return true }

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        view.addSubview(capturePreviewView)
        view.pinToBounds(capturePreviewView)
        view.addSubview(imagePreview)
        view.pinToBounds(imagePreview)
        view.addSubview(moviePreview)
        view.pinToBounds(moviePreview)
        view.addSubview(noCameraAccessView)
        NSLayoutConstraint.activate([
            noCameraAccessView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            noCameraAccessView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            noCameraAccessView.heightAnchor.constraint(equalToConstant: CreateReportNoCameraAccessView.height),
            noCameraAccessView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if view.safeAreaInsets.bottom > 0 {
            imagePreview.setupCornerRadius()
            moviePreview.setupCornerRadius()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad?()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        presenter.viewWillDisappear?()
    }
}

// MARK: - ICreateReportCameraViewController
extension CreateReportCameraViewController: ICreateReportCameraViewController {
    func show(state: State) {
        imagePreview.isHidden = true
        moviePreview.isHidden = true
        noCameraAccessView.isHidden = true
        switch state {
        case .imagePreview(let image):
            presenter.stopCameraSession()
            imagePreview.setup(with: image)
            imagePreview.isHidden = false
        case .moviePreview(let path):
            presenter.stopCameraSession()
            moviePreview.isHidden = false
            moviePreview.configure(videoURL: path)
        case .noAccess:
            presenter.stopCameraSession()
            noCameraAccessView.isHidden = false
            showAlertForSettings()
        case .camera:
            presenter.startCameraSession()
        }
    }

    func showAlertForSettings() {
        let cameraUnavailableAlertController = UIAlertController(title: "Setting",
                                                                 message: "Need access to camera and microphone for capture a video",
                                                                 preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: "Settings",
                                           style: .destructive) { _ in
            let settingsUrl = URL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        present(cameraUnavailableAlertController, animated: true, completion: nil)
    }

    func setupPreview(with image: UIImage) {
        imagePreview.setup(with: image)
        imagePreview.isHidden = false
    }

    func setupPreview(with movie: URL) {
        moviePreview.isHidden = false
        moviePreview.configure(videoURL: movie)
    }

    func hidePreview(url: URL?) {
        imagePreview.setup(with: nil)
        imagePreview.isHidden = true
        moviePreview.isHidden = true
    }

    func getCapturePreviewView() -> UIView {
        return capturePreviewView.capturePreviewView
    }

    func setupFlashButtonWith(image: UIImage?) {
        capturePreviewView.setupFlashButton(with: image)
    }

    func showPhotoLibrary() {
        DispatchQueue.main.async {
            AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
            AttachmentHandler.shared.imagePickedBlock = { [weak self] image in
                self?.presenter.setImageFromLibrary(image)
            }
            AttachmentHandler.shared.videoPickedBlock = { [weak self] url in
                self?.presenter.setUrlFromLibrary(url)
            }
        }
    }
}
