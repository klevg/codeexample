//
//  NoCameraAccessView.swift
//  RadiusReport
//
//  Created by Eugene on 3/4/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

protocol CreateReportNoCameraAccessViewDelegate: class {
    func didTapOnEnableCameraButton()
}

final class CreateReportNoCameraAccessView: XibView {
    static let height: CGFloat = 135

    // MARK: - Outlets
    @IBOutlet private weak var enableCameraButton: UIButton!
    @IBOutlet private weak var mainTitleLabel: UILabel!

    weak var delegate: CreateReportNoCameraAccessViewDelegate?

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        enableCameraButton.setTitle("Enable Camera", for: .normal)
        mainTitleLabel.text = "Please enable access to your camera"
        enableCameraButton.layer.cornerRadius = 25.5
    }

    // MARK: - Actions
    @IBAction private func enableCameraButtonTapped() {
        delegate?.didTapOnEnableCameraButton()
    }
}
