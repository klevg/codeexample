//
//  CreateReportWithMediaController.swift
//  RadiusReport
//
//  Created by Eugene on 3/3/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation
import AVFoundation

protocol ICreateReportWithMediaController: IBaseCreateReportController {
    func setup(with image: UIImage?)
    func setup(with url: URL)
}

final class CreateReportWithMediaController: BaseCreateReportController,
                                             ICreateReportWithMediaController {

    var presenter: ICreateReportWithMediaPresenter!

    var mediaHeightConstraint: NSLayoutConstraint!
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var moviePreview: CreateReportMoviePreviewView = {
        let view = CreateReportMoviePreviewView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: - Lifecycle
    override func loadView() {
        super.loadView()
        setupLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setHeadlineViewConfig(view: headlineView)
        presenter.setLocationViewConfig(view: addressView)
        presenter.viewDidLoad?()
    }

    private func setupLayout() {
        contentView.addSubview(imageView)
        mediaHeightConstraint = imageView.heightAnchor.constraint(equalToConstant: 100)
        mediaHeightConstraint.isActive = true
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: headlineView.bottomAnchor),
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])

        contentView.addSubview(moviePreview)
        NSLayoutConstraint.activate([
            moviePreview.topAnchor.constraint(equalTo: headlineView.bottomAnchor),
            moviePreview.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            moviePreview.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            moviePreview.heightAnchor.constraint(equalTo: imageView.heightAnchor)
        ])
    }

    @objc override func leftBarButtonTapped() {
        presenter.onCancel?()
    }

    @objc override func rightBarButtonTapped() {
        presenter.sendReport()
    }

    func setup(with image: UIImage?) {
        moviePreview.isHidden = true
        guard let imageHeight = image?.size.height,
            let imageWidth = image?.size.width else { return }
        mediaHeightConstraint.constant = imageHeight * getRatio(basedOn: imageWidth)
        imageView.image = image
    }

    func setup(with url: URL) {
        imageView.isHidden = true
        guard let videoSize = resolutionForLocalVideo(url: url) else { return }
        mediaHeightConstraint.constant = videoSize.height * getRatio(basedOn: videoSize.width)
        moviePreview.configure(videoURL: url)
    }

    private func getRatio(basedOn width: CGFloat) -> CGFloat {
        let viewWidth = view.frame.width
        var scale: CGFloat = 1
        if width > viewWidth {
            scale = viewWidth / width
        }
        return scale
    }

    private func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: .video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}
