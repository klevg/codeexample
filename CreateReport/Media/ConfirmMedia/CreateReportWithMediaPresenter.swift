//
//  CreateReportWithMediaPresenter.swift
//  RadiusReport
//
//  Created by Eugene on 3/3/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

protocol ICreateReportWithMediaPresenter: IBaseCreateReportPresenter { }

final class CreateReportWithMediaPresenter: BaseCreateReportPresenter, ICreateReportWithMediaPresenter {
    private weak var view: ICreateReportWithMediaController?
    private let reportImage: UIImage?
    private let movieURL: URL?

    // MARK: - Initialization
    init(view: ICreateReportWithMediaController,
         reportImage: UIImage?,
         movieURL: URL?) {
        self.view = view
        self.reportImage = reportImage
        self.movieURL = movieURL
    }

    // MARK: - Lifecycle
    func viewDidLoad() {
        view?.setTitle(with: "Post Report")

        if locationService.isGranted {
            locationService.onUpdateLocation = { [weak self] location in
                self?.locationService.stopUpdatingLocation()
                self?.locationService.geocode(coordinate: location.coordinate) { (locationString) in
                    let model = LocationModel(location: locationString ?? "",
                                              latitude: String(location.coordinate.latitude),
                                              longitude: String(location.coordinate.longitude),
                                              isCurrentLocation: true)
                    self?.updateModel(model: model)
                }
            }
            locationService.startUpdatingLocation()
        } else {
            view?.updateLocationTitle(with: "no location")
        }

        if let reportImage = reportImage {
            view?.setup(with: reportImage)
        } else if let movieURL = movieURL {
            view?.setup(with: movieURL)
        }
    }

    override func sendReport() {
        view?.showHUD()
        var media = [(name: String, data: Data)]()
        if let reportImage = reportImage,
            let data = reportImage.pngData() {
            media.append((name: "image",
                          data: data))
        } else if let movieURL = movieURL,
            let data = try? Data(contentsOf: movieURL) {
            media.append((name: "movie",
                          data: data))
        }

        let model = ReportRequest(title: headlineString,
                                  fullText: nil,
                                  link: nil,
                                  location: location ?? "",
                                  latitude: latitude,
                                  longitude: longitude,
                                  type: ReportType.media.index,
                                  deletedMedia: nil,
                                  media: media)
        interactor.send(model: model)
    }

    override func updateModel(model: LocationModel) {
        super.updateModel(model: model)
        let locationTitle = model.isCurrentLocation ? "Current Location" : model.location
        view?.updateLocationTitle(with: locationTitle)
    }
}

// MARK: - CreateReportInteractorOutput
extension CreateReportWithMediaPresenter: CreateReportInteractorOutput {
    func updateProgress(with: Double) {
        view?.showHUDWithProgress(with, title: "Uploading")
    }

    func success() {
        view?.hideHUD()
        onSelect?()
    }

    func failure() {
        view?.hideHUD()
        view?.showErrorAlert()
    }
}
