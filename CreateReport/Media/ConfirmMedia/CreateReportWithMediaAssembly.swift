//
//  CreateReportWithMediaAssembly.swift
//  RadiusReport
//
//  Created by Eugene on 3/3/20.
//  Copyright © 2020 Mark Vialichka. All rights reserved.
//

import Foundation

final class CreateReportWithMediaAssembly {
    struct Module {
        let view: CreateReportWithMediaController
        let presenter: CreateReportWithMediaPresenter
    }

    private init() {}

    static func build(reportImage: UIImage?, movieURL: URL?, reportRepository: ReportRepository) -> Module {
        let view = CreateReportWithMediaController()
        let presenter = CreateReportWithMediaPresenter(view: view, reportImage: reportImage, movieURL: movieURL)
        view.presenter = presenter
        let interactor = CreateReportInteractor(reportRepository: reportRepository)
        interactor.output = presenter
        presenter.interactor = interactor
        return .init(view: view, presenter: presenter)
    }
}
